The Fred Moheban Gallery is a fine purveyor of antique rugs and carpets. For over 50 years, our passion has been to find the rare antique rug or carpet that connotes the true artist, sometimes in those pieces like room-sized jewel, or even a small prayer rug.

Address: 16 E 52nd St, New York, NY 10022, USA

Phone: 212-397-9060

Website: https://www.fredmoheban.com
